package com.safebear.app;

import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class Test01_Login extends BaseTest {

    @Test
    public void testLogin() {
        // Confirm that the browser session opens on the welcome page
        assertTrue(welcomePage.checkCorrectPage());

        // Click the login link (which returns true if on the expected page)
        assertTrue(welcomePage.clickOnLogin(loginPage));

        loginPage.signIn();
        assertTrue(userPage.checkCorrectPage());
    }
}
