package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

public class UserPage {
    private WebDriver driver;

    // Construct. Initialize the driver and any WebElements.
    public UserPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    // Check that User page has the title 'Logged In'
    public boolean checkCorrectPage() {
        return driver.getTitle().matches("Logged In");
    }
}
