package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class WelcomePage {
    private WebDriver driver;

    @FindBy(linkText = "Login")
    private WebElement loginLink;

    // Construct. Initialize the driver and any WebElements.
    public WelcomePage(WebDriver driver)
    {
        this.driver = driver;
        PageFactory.initElements(driver,this);
    }

    // Confirm that Welcome Page has a title
    public boolean checkCorrectPage(){
        return driver.getTitle().matches("Welcome");
    }

    // Click the loginLink (i.e. linkText = "Login") and confirm it loads the correct page
    public boolean clickOnLogin(LoginPage loginPage)
    {
        loginLink.click();
        return loginPage.checkCorrectPage();
    }
}
