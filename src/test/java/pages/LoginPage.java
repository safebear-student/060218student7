package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LoginPage {
    private WebDriver driver;

    @FindBy(id = "myid")
    private WebElement userIdText;

    @FindBy(id = "mypass")
    private WebElement userPassText;

    @FindBy(xpath = "//form[@name=\"login\"]/button[@type=\"submit\"]")
    private WebElement signInButton;

    // Construct. Initialize the driver and any WebElements.
    public LoginPage(WebDriver driver)
    {
        this.driver = driver;
        PageFactory.initElements(driver,this);
    }

    // Click the signInButton (i.e. id = "myid") and confirm it loads the correct page
    public void signIn()
    {
        userIdText.sendKeys("testuser");
        userPassText.sendKeys("testing");
        signInButton.click();
    }

    // Check that Login page has the title 'Sign In'
    boolean checkCorrectPage() {
        return driver.getTitle().matches("Sign In");
    }
}
